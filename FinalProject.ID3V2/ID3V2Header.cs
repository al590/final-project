﻿///////////////////////////////////////////////////////////////////////////////
// ID3V2Header.cs
//
// Author      : Adam Likuski
// Description : Class for storing data relevant to a ID3V2 tag
///////////////////////////////////////////////////////////////////////////////
using System.IO;

namespace FinalProject.ID3V2
{
    /// <summary>
    /// An ID3V2 tag header.
    /// </summary>
    public class ID3V2Header
    {
        #region Constants 

        /// <summary>
        /// The header for every ID3V2 tag.
        /// </summary>
        private static readonly byte[] Header = { 0x49, 0x44, 0x33 };

        #endregion

        #region Properties 

        /// <summary>
        /// The major version of the ID3V2 tag. This should always be 2, as 
        /// anything other than 2 would show an unsupported ID3 tag version.
        /// </summary>
        public byte MajorVersion { get; set; }

        /// <summary>
        /// The revision number (minor version) of the ID3V2 tag.
        ///
        /// This will often be between 0 - 4, however as each version has its
        /// own changes to the spec it is possible this version isn't fully
        /// supported for parsing.
        /// </summary>
        public byte Revision { get; set; }

        /// <summary>
        /// The flags for the ID3V2 tag (often will be 0 with no flags set).
        /// </summary>
        public byte Flags;

        #endregion

        #region Constructors

        public ID3V2Header(byte majorVersion, byte revision, byte flags)
        {
            MajorVersion = majorVersion;
            Revision = revision;
            Flags = flags;
        }

        public void Write(ref BinaryWriter writer)
        {
            writer.Write(Header); 
            writer.Write(MajorVersion);
            writer.Write(Revision);
            writer.Write(Flags);
        }

        #endregion
    }
}
