﻿///////////////////////////////////////////////////////////////////////////////
// Util.cs
//
// Author      : Adam Likuski
// Description : Provides helper methods and extension methods
///////////////////////////////////////////////////////////////////////////////
using System;

namespace FinalProject.ID3V2
{
    public static class Util
    {
        /// <summary>
        /// Unsychronizes an integer from the bytes provided. Bytes must be in
        /// MSB format, as per the ID3V2 spec.
        /// </summary>
        /// <param name="data">the bytes of the synchronized integer in MSB format</param>
        /// <returns>the unsynchronized integer</returns>
        public static int Unsync(byte[] data) => 
            // Data MUST be in MSB format!
            (data[0] << 21) | 
            (data[1] << 14) | 
            (data[2] << 7 ) | 
            data[3];

        /// <summary>
        /// Sychronizes an integer.
        /// </summary>
        /// <param name="value">the unsynchronized integer</param>
        /// <returns>the synchronized version of the integer</returns>
        public static int Sync(int value) => 
            // 127 for all bits EXCEPT MSB!!! (0b01111111)
            (value & 127)                 |
            (((value >> 7) & 127) << 8)   |
            (((value >> 14) & 127) << 16) |
            (((value >> 21) & 127) << 24);

        /// <summary>
        /// Checks if the data provided starts with a Unicode byte order mark.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>true if the data starts with a Unicode BOM, false otherwise</returns>
        public static bool HasUnicodeBOM(byte[] data) => 
            data[0] == 0xFE && data[1] == 0xFF ||
            data[0] == 0xFF && data[1] == 0xFE;

        /// <summary>
        /// Checks to see whether this encoding is currently supported or not.
        ///
        /// More of a sanity check method.
        /// </summary>
        /// <param name="encoding">the encoding used (from ID3V2 tag, etc.)</param>
        /// <returns>true always; will throw an exception if not supported</returns>
        public static bool CheckValidEncoding(byte encoding)
        {
            if (encoding != 0 && encoding != 1)
                throw new ArgumentException(
                    $"Unsupported encoding {encoding} detected!");

            return true;
        }

        /// <summary>
        /// Returns the size of the string in bytes.
        /// </summary>
        /// <param name="str">the string</param>
        /// <param name="encoding">the encoding the string uses</param>
        /// <returns>the size, in bytes, of the string</returns>
        public static int GetStringSize(string str, byte encoding)
        {
            if (string.IsNullOrEmpty(str))
                return 0;
            return encoding == 0 ? str.Length : str.Length * 2;
        }

        /// <summary>
        /// Returns the size of the string in bytes, include its null 
        /// terminator as part of the size.
        /// </summary>
        /// <param name="str">the string</param>
        /// <param name="encoding">the encoding the string uses</param>
        /// <returns>the size, in bytes, of the string plus its null terminator</returns>
        public static int GetStringSizeNT(string str, byte encoding)
        {
            // Only the NT for default,
            // Unicode BOM + two bytes for NT for Unicode
            var size = encoding == 0 ? 1 : 4;
            if (!string.IsNullOrEmpty(str))
                size += (encoding == 0 ? str.Length + 1 : str.Length * 2 + 4);
            return size;
        }
    }
}
