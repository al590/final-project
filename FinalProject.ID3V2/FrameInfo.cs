﻿///////////////////////////////////////////////////////////////////////////////
// FrameInfo.cs
//
// Author      : Adam Likuski
// Description : Provides ID3V2 frame related data
///////////////////////////////////////////////////////////////////////////////
using System.Collections.Generic;

namespace FinalProject.ID3V2
{
    public static class FrameInfo
    {
        //
        // Constants for easier access
        //
        public static readonly string
            ALBUM = "Album",
            ATTACHED_PICTURE = "Attached Picture",
            ARTIST = "Artist",
            GENRE = "Genre",
            TRACK_NUMBER = "Track Number",
            TRACK_NAME = "Track Title",
            YEAR = "Year";

        //
        // Some frames are generalized
        //
        public static readonly Dictionary<string, string> IDMapping = new Dictionary<string, string>
        {
            {"APIC", ATTACHED_PICTURE},
            {"COMM", "Comments"},
            {"TALB", ALBUM},
            {"TCON", GENRE},
            {"TCOP", "Copyright Message"},
            {"TDRC", YEAR}, // "Recording Time"},
            {"TIT2", TRACK_NAME},
            {"TIT3", "Subtitle"},
            {"TPE1", ARTIST},
            {"TPE2", ARTIST},
            {"TPOS", TRACK_NUMBER},
            {"TPUB", "Publisher"},
            {"TRCK", TRACK_NUMBER},
            {"TXXX", "User Text Info"},
            {"TYER", YEAR}
        };

        /// <summary>
        /// Helper method to lookup the human-readable name for the frame ID.
        /// </summary>
        /// <param name="frameID">the id of the frame, as per the ID3V2 spec</param>
        /// <returns>the human readable name of the frame or null</returns>
        public static string GetName(string frameID) => 
            IDMapping.ContainsKey(frameID) ? IDMapping[frameID] : null;
    }
}
