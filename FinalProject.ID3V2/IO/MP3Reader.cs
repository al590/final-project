﻿///////////////////////////////////////////////////////////////////////////////
// MP3Reader.cs
//
// Author      : Adam Likuski
// Description : Provides a way of reading ID3V2 data from MP3 files
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.IO;
using FinalProject.ID3V2.Frames;

namespace FinalProject.ID3V2.IO
{
    /// <summary>
    /// Provides a way to read MP3 file data, specifically the ID3V2 tag data
    /// from the MP3 file.
    /// </summary>
    public class MP3Reader : IDisposable
    {
        #region Fields

        private BinaryReader _reader;

        #endregion

        #region Constructors

        public MP3Reader(string path)
            : this(File.OpenRead(path))
            { }

        public MP3Reader(Stream stream) => 
            _reader = new BinaryReader(stream);

        #endregion

        /// <summary>
        /// Reads the ID3V2 tag data from a file.
        /// </summary>
        /// <returns></returns>
        public MP3File Read()
        {
            if (_reader == null)
                throw new InvalidOperationException("Reader has already been closed!");

            // Start reading from the beginning of the file
            _reader.BaseStream.Position = 0;

            var header = _reader.ReadBytes(3);
            var version = _reader.ReadBytes(2);
            var flags = _reader.ReadByte();
            var size = _reader.ReadSynchronizedInt();

            // Ensure this is an ID3V2 tagged file
            if (header[0] != 'I' || header[1] != 'D' || header[2] != '3' ||
                version[0] == 0xFF || version[1] == 0xFF || flags >= 0x80)
                throw new ArgumentException("Not an valid ID3V2 tagged file!");
            else if (version[0] != 3) 
                throw new ArgumentException("Unsupported ID3 tag version!");

            var frames = new List<Frame>();
            // Size is the size of the tag's content so we need to account for
            // the initial 10 bytes in the stream for the ID3V2 header
            var end = 10 + size;

            while (_reader.BaseStream.Position < end)
            {
                var nextByte = _reader.PeekBytes(1);

                // There is a chance the file has padding; if this is padding, 
                // ignore it
                if (nextByte[0] == 0)
                {
                    // TODO: It might be worth preserving the padding as right 
                    // now we aren't, though it is not required
                    _reader.BaseStream.Position += 1;
                    continue;
                }

                var frame = ReadFrame();

                if (frame != null)
                    frames.Add(frame);
            }

            var id3V2Header = new ID3V2Header(version[0], version[1], flags);
            // TODO: This works for smaller files, but if this was ever expanded this would need to be read in chunks
            // (just look at the int cast)
            var data = _reader.ReadBytes((int) (_reader.BaseStream.Length - _reader.BaseStream.Position));
            return new MP3File(id3V2Header, size, frames, data);
        }

        /// <summary>
        /// Reads and parses a frame from the file. Will throw a 
        /// NotImplementedException if the reader comes across an unsupported
        /// frame.
        /// </summary>
        /// <returns>the parsed frame</returns>
        protected Frame ReadFrame()
        {
            var id = Encodings.Default.GetString(_reader.PeekBytes(4));

            // Anything that starts with T is a text frame
            if (id.StartsWith("T"))
                return new TextFrame(ref _reader);
            else if (id.Equals("APIC"))
                return new AttachedPictureFrame(ref _reader);
            // Comment frames should eventually be supported as they are fairly
            // common
            else
                throw new NotImplementedException($"Unsupported frame {id} detected!");
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _reader?.Close();
            _reader = null;
        }
    }
}
