﻿///////////////////////////////////////////////////////////////////////////////
// MP3Writer.cs
//
// Author      : Adam Likuski
// Description : Provides a way of write ID3V2 data to MP3 files
///////////////////////////////////////////////////////////////////////////////
using System;
using System.IO;

namespace FinalProject.ID3V2.IO
{
    /// <summary>
    /// Provides a way to write data to an MP3 file, specifically the ID3V2 tag data
    /// from the MP3 file.
    /// </summary>
    public class MP3Writer : IDisposable
    {
        #region Fields

        private BinaryWriter _writer;

        #endregion

        #region Constructors

        public MP3Writer(string path) 
            : this(File.OpenWrite(path)) 
            { }

        public MP3Writer(Stream stream) => 
            _writer = new BinaryWriter(stream);

        #endregion

        /// <summary>
        /// Writes the ID3V2 tag data to a file.
        /// <param name="file">the MP3 file to write</param>
        /// </summary>
        public void Write(MP3File file)
        {
            if (file.Frames == null || file.Frames.Count < 1)
                throw new ArgumentException("MP3File must have at least one " +
                    "frame and not be null!");

            // ID3V2 header (ID3, version, revision)
            file.Header.Write(ref _writer); 

            // Calculate the entire size of the ID3V2 tag
            var size = 0;

            foreach (var frame in file.Frames)
                size += frame.TotalSize;

            // The value needs to be synchronized
            _writer.WriteSynchronizedInt(size);

            foreach (var frame in file.Frames)
                frame.Write(ref _writer);

            if (file.Data != null)
                _writer.Write(file.Data);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _writer?.Close();
            _writer = null;
        }
    }
}
