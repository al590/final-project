﻿///////////////////////////////////////////////////////////////////////////////
// IOUtil.cs
//
// Author      : Adam Likuski
// Description : Provides helper methods and extension methods for IO related 
//               operations.
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.IO;

namespace FinalProject.ID3V2.IO
{
    /// <summary>
    /// Provides extension methods and helper methods for IO related 
    /// operations.
    /// </summary>
    public static class IOUtil
    {
        /// <summary>
        /// The byte-order-mark for a unicode string. Used when working with
        /// Unicode-encoded data.
        /// </summary>
        private static readonly byte[] UnicodeBOM = { 0xFF, 0xFE };

        /// <summary>
        /// Reverses bytes before using the BitConverter if the BitConverter
        /// is Little Endian.
        /// </summary>
        /// <param name="bytes">the data to reverse</param>
        internal static void CheckReverse(ref byte[] bytes)
        {
            if (BitConverter.IsLittleEndian)
                Array.Reverse(bytes);
        }

        /// <summary>
        /// Reads an integer in Big Endian format from a BinaryReader.
        /// </summary>
        /// <param name="reader">the stream to use</param>
        /// <returns>the converted integer</returns>
        internal static int ReadBEInt32(this BinaryReader reader)
        {
            // bytes will be MSB but BitConverter may not be so check reverse
            var bytes = reader.ReadBytes(4);
            CheckReverse(ref bytes);
            return BitConverter.ToInt32(bytes, 0);
        }

        /// <summary>
        /// Reads a synchronized integer (see ID3V2 spec) from a BinaryReader.
        /// </summary>
        /// <param name="reader">the stream to use</param>
        /// <returns>the converted integer</returns>
        public static int ReadSynchronizedInt(this BinaryReader reader)
        {
            // bytes will ALREADY be in MSB and we're not using BitConverter
            // so no need to reverse!
            var bytes = reader.ReadBytes(4);
            // Need to unsynchronize the integer so we can use its original 
            // value
            return Util.Unsync(bytes);
        }

        /// <summary>
        /// Writes an unsynchronized integer to a stream as a synchronized 
        /// integer.
        /// </summary>
        /// <param name="writer">the stream to write to</param>
        /// <param name="value">the unsynchronized value to write</param>
        public static void WriteSynchronizedInt(this BinaryWriter writer, int value)
        {
            // Value is UNSYCHRONIZED so we need to sync it first
            var sync = Util.Sync(value);
            var bytes = BitConverter.GetBytes(sync);
            // Bytes may not be in MSB if we're using a LE bitconverter
            CheckReverse(ref bytes);
            writer.Write(bytes);
        }

        /// <summary>
        /// Reads an short in Big Endian format from a BinaryReader.
        /// </summary>
        /// <param name="reader">the stream to use</param>
        /// <returns>the converted integer</returns>
        public static short ReadBEShort(this BinaryReader reader)
        {
            var bytes = reader.ReadBytes(2);
            // Bytes will be MSB but BitConverter may not be so check reverse
            CheckReverse(ref bytes);
            return BitConverter.ToInt16(bytes, 0);
        }

        /// <summary>
        /// Writes an short in Big Endian format to a stream.
        /// </summary>
        /// <param name="writer">the stream to write to</param>
        /// <param name="value">the value to write</param>
        public static void WriteBEShort(this BinaryWriter writer, short value)
        {
            var bytes = BitConverter.GetBytes(value);
            // bytes will be MSB but BitConverter may not be so check reverse
            CheckReverse(ref bytes);
            writer.Write(bytes);
        }

        public static string ReadString(this BinaryReader reader, byte encoding,
            int size)
        {
            Util.CheckValidEncoding(encoding);

            // byte order is irrelevant for string data 
            // (not using BitConverter)
            var bytes = reader.ReadBytes(size);

            if (encoding == 0)
                return Encodings.Default.GetString(bytes);
            else if (encoding == 1)
                return Util.HasUnicodeBOM(bytes)
                    ? Encodings.Unicode.GetString(bytes, 2, bytes.Length - 2)
                    : Encodings.Unicode.GetString(bytes);

            // Leaving room for supporting other encodings (eventually?)
            return null;
        }

        /// <summary>
        /// Writes a string without its length to a stream.
        /// </summary>
        /// <param name="writer">the stream to write to</param>
        /// <param name="encoding">the encoding to use for writing the text</param>
        /// <param name="text">the text to write</param>
        public static void WriteString(ref BinaryWriter writer, byte encoding, string text)
        {
            Util.CheckValidEncoding(encoding);

            if (encoding == 0)
                writer.Write(Encodings.Default.GetBytes(text));
            else if (encoding == 1)
            {
                // Write Unicode BOM first
                writer.Write(UnicodeBOM);
                // byte order is irrelevant for string data (not using BitConverter)
                writer.Write(Encodings.Unicode.GetBytes(text));
            }
        }

        public static string ReadNTString(this BinaryReader reader, 
            out int bytesRead, byte encoding)
        {
            Util.CheckValidEncoding(encoding);

            var startPos = reader.BaseStream.Position;
            var len = reader.BaseStream.Length;
            var bytes = new List<byte>();

            if (encoding == 0)
            {
                byte b;

                // Non-unicode requires only one \0 to terminate the string.
                while ((b = reader.ReadByte()) != 0)
                    bytes.Add(b);
            }
            else if (encoding == 1)
            {
                var b = reader.PeekBytes(2);

                // Skip unicode BOM if present
                if (Util.HasUnicodeBOM(b))
                    reader.BaseStream.Position += 2;

                while (reader.BaseStream.Position < len - 1)
                {
                    b = reader.ReadBytes(2);

                    // NT is two bytes for unicode
                    if (b[0] == 0 && b[1] == 0)
                        break;

                    bytes.AddRange(b);
                }
            }

            // How many bytes we advanced (INCLUDES THE NULL TERMINATOR)
            bytesRead = (int) (reader.BaseStream.Position - startPos);

            if (bytes.Count == 0)
                return "";
            else if (encoding == 0)
                return Encodings.Default.GetString(bytes.ToArray());
            else if (encoding == 1)
                return Encodings.Unicode.GetString(bytes.ToArray());

            // Leaving room for supporting other encodings (eventually?)
            return null;
        }

        public static void WriteNTString(this BinaryWriter writer, 
            byte encoding, string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                if (encoding != 0)
                    // Write BOM if Unicode
                    writer.Write(UnicodeBOM);

                writer.Write(encoding == 0 
                    ? Encodings.Default.GetBytes(text) 
                    : Encodings.Unicode.GetBytes(text));
            }
            else
                // Still unsure whether or not the BOM is required for empty
                // strings... some files have it while others don't.
                if (encoding != 0)
                    writer.Write(UnicodeBOM);

            // Write NT
            writer.Write((byte)0);

            // Unicode requires two bytes for NT
            if (encoding != 0)
                writer.Write((byte)0);
        }

        /// <summary>
        /// Writes an integer in Big Endian format to a stream.
        /// </summary>
        /// <param name="writer">the stream to write to</param>
        /// <param name="value">the value to write</param>
        public static void WriteBEInt(this BinaryWriter writer, int value)
        {
            var bytes = BitConverter.GetBytes(value);
            // bytes will be MSB but BitConverter may not be so check reverse
            CheckReverse(ref bytes);
            writer.Write(bytes);
        }

        /// <summary>
        /// Extension method to read a specified number of bytes without
        /// advancing the current stream position.
        /// </summary>
        /// <param name="reader">the stream to read from</param>
        /// <param name="count">the amount of bytes to read</param>
        /// <returns>the bytes that will follow in subsequent read calls</returns>
        public static byte[] PeekBytes(this BinaryReader reader, int count)
        {
            var bytes = reader.ReadBytes(count);
            reader.BaseStream.Position -= count;
            return bytes;
        }
    }
}
