﻿///////////////////////////////////////////////////////////////////////////////
// Encodings.cs
//
// Author      : Adam Likuski
// Description : Stores encoding related constants for convenience
///////////////////////////////////////////////////////////////////////////////
using System.Text;

namespace FinalProject.ID3V2
{
    /// <summary>
    /// Stores common encodings used while parsing ID3V2 tagged files.
    /// </summary>
    internal class Encodings
    {
        /// <summary>
        /// The default, non-unicode encoding (ISO-8859-1, as per the ID3V2 spec)
        /// </summary>
        public static readonly Encoding Default = Encoding.GetEncoding("ISO-8859-1");

        /// <summary>
        /// Alias for Encoding.Unicode stored here for consistency.
        /// </summary>
        public static readonly Encoding Unicode = Encoding.Unicode;
    }
}
