﻿///////////////////////////////////////////////////////////////////////////////
// TextFrame.cs
//
// Author      : Adam Likuski
// Description : Frame to handling parsing of text frames
///////////////////////////////////////////////////////////////////////////////
using System;
using System.IO;
using FinalProject.ID3V2.IO;

namespace FinalProject.ID3V2.Frames
{
    /// <summary>
    /// Frame built to handle mostly all text-based frames in the ID3V2 spec.
    /// </summary>
    public sealed class TextFrame : Frame
    {
        #region Fields

        private string _text;

        #endregion

        #region Properties

        public byte Encoding { get; set; }

        public string Text
        {
            get => _text;
            set { _text = value; UpdateSize(); }
        }

        #endregion

        #region Constructors

        public TextFrame(string id) =>
            ID = id;

        public TextFrame(ref BinaryReader reader) => 
            Parse(ref reader);

        #endregion

        /// <inheritdoc />
        public override void Parse(ref BinaryReader reader)
        {
            base.Parse(ref reader);
            Encoding = reader.ReadByte();
            Util.CheckValidEncoding(Encoding);
            Text = reader.ReadString(Encoding, Size - 1);
        }

        /// <inheritdoc />
        public override void Write(ref BinaryWriter writer)
        {
            if (string.IsNullOrEmpty(Text))
                throw new InvalidOperationException("Text must not be empty!");

            base.Write(ref writer);
            writer.Write(Encoding);
            IOUtil.WriteString(ref writer, Encoding, Text);
        }

        /// <inheritdoc />
        protected override void UpdateSize()
        {
            Size = 1 + Util.GetStringSize(Text, Encoding);

            // Include the size of the Unicode BOM
            if (Encoding > 0)
                Size += 2;
        }
    }
}
