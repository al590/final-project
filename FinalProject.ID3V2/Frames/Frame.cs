﻿///////////////////////////////////////////////////////////////////////////////
// Frame.cs
//
// Author      : Adam Likuski
// Description : Base class for a generic ID3V2 frame. Other frames will extend
//               this class and implement more specific functionality based on
//               the frame it is made for.
///////////////////////////////////////////////////////////////////////////////
using System;
using System.IO;
using FinalProject.ID3V2.IO;

namespace FinalProject.ID3V2.Frames
{
    /// <summary>
    /// Represents a generic ID3V2 frame.
    /// </summary>
    public class Frame
    {
        #region Fields

        //
        // Backing fields for properties
        //
        private string _id;

        #endregion

        #region Properties 

        /// <summary>
        /// A human-readable version of the frame's ID.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The size, in bytes, of the frame.
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// The flags of the frame. Limited implementation.
        /// </summary>
        public short Flags { get; set; }

        /// <summary>
        /// Returns the total size of the frame including the size of the frame
        /// header.
        /// </summary>
        public int TotalSize => Size + 10;

        /// <summary>
        /// The id, according to the ID3V2 spec, of the frame.
        /// </summary>
        public string ID
        {
            get => _id;
            set
            {
                // Automatically update the name of the frame
                _id = value;
                Name = FrameInfo.GetName(value);
            }
        }

        #endregion

        /// <summary>
        /// Recalculates the total size, in bytes, of the frame.
        /// </summary>
        protected virtual void UpdateSize()
        {
            throw new NotImplementedException();    
        }

        /// <summary>
        /// Parses frame data from a stream.
        /// </summary>
        /// <param name="reader">the stream to use</param>
        public virtual void Parse(ref BinaryReader reader)
        {
            ID = Encodings.Default.GetString(reader.ReadBytes(4));
            Size = reader.ReadSynchronizedInt();
            Flags = reader.ReadBEShort();
        }

        /// <summary>
        /// Writes the frame's information to a stream.
        /// </summary>
        /// <param name="writer">the stream to use</param>
        public virtual void Write(ref BinaryWriter writer)
        {
            writer.Write(Encodings.Default.GetBytes(ID));
            writer.WriteSynchronizedInt(Size);
            writer.WriteBEShort(Flags);
        }
    }
}
