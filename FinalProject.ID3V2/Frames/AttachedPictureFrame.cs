﻿///////////////////////////////////////////////////////////////////////////////
// AttachedPictureFrame.cs
//
// Author      : Adam Likuski
// Description : Frame to handling parsing of the attached picture (APIC) frame
///////////////////////////////////////////////////////////////////////////////
using System.IO;
using FinalProject.ID3V2.IO;

namespace FinalProject.ID3V2.Frames
{
    /// <summary>
    /// Frame for the ID3V2 tag "APIC".
    /// </summary>
    public sealed class AttachedPictureFrame : Frame
    {
        #region Fields

        //
        // Backing fields for properties that need to recalculate the size of
        // the image
        //
        private string _mimeType;
        private string _description;
        private byte[] _data;

        #endregion

        #region Properties

        /// <summary>
        /// The encoding that is used for the description for this image.
        /// </summary>
        public byte Encoding { get; set; }

        /// <summary>
        /// What type of picture this is. 
        /// </summary>
        public byte PictureType { get; set; }

        /// <summary>
        /// The mime type for this image, such as "image/jpeg", etc.
        /// </summary>
        public string MimeType
        {
            get => _mimeType;
            set { _mimeType = value; UpdateSize(); }
        }

        /// <summary>
        /// An optional description for this image.
        /// </summary>
        public string Description
        {
            get => _description;
            set { _description = value; UpdateSize(); }
        }

        /// <summary>
        /// The actual data (bytes) of the image.
        /// </summary>
        public byte[] Data
        {
            get => _data;
            set { _data = value; UpdateSize(); }
        }

        #endregion

        #region Constructors

        public AttachedPictureFrame() =>
            // Require as ID is null by default
            ID = "APIC";

        public AttachedPictureFrame(ref BinaryReader reader) 
            : this() 
            => Parse(ref reader);

        #endregion

        /// <inheritdoc />
        public override void Parse(ref BinaryReader reader)
        {
            // We can't rely on the generic parsing for images as their
            // size is not stored as a synchronized integer
            ID = Encodings.Default.GetString(reader.ReadBytes(4));
            var size = reader.ReadBEInt32();
            Flags = reader.ReadBEShort();
            Encoding = reader.ReadByte();
            MimeType = reader.ReadNTString(out var readMime, 0);
            PictureType = reader.ReadByte();
            Description = reader.ReadNTString(out var readDesc, Encoding);
            Data = reader.ReadBytes(size - (2 + readMime + readDesc));

            // The properties will overwrite Size because of UpdateSize() so
            // restore the original value
            Size = size;
        }

        /// <inheritdoc />
        public override void Write(ref BinaryWriter writer)
        {
            // Same as Parse() - Size is NOT synchronized
            // Size will always be current because of the properties
            writer.Write(Encodings.Default.GetBytes(ID));
            writer.WriteBEInt(Size);
            writer.WriteBEShort(Flags);
            writer.Write(Encoding);
            writer.WriteNTString(0, MimeType);
            writer.Write(PictureType);
            writer.WriteNTString( Encoding, Description);
            writer.Write(Data);
        }

        /// <inheritdoc />
        protected override void UpdateSize()
        {
            // Encoding + PictureType
            Size = 2;

            // Mime Type + NT
            Size += Util.GetStringSizeNT(MimeType, 0); 

            // Description (encoding based) + NT
            Size += Util.GetStringSizeNT(Description, Encoding); 

            if (Data != null)
                Size += Data.Length;
        }
    }
}
