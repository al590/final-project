﻿///////////////////////////////////////////////////////////////////////////////
// MP3File.cs
//
// Author      : Adam Likuski
// Description : Provides a class to model a basic MP3 file and its ID3V2 tag
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using FinalProject.ID3V2.Frames;

namespace FinalProject.ID3V2
{
    /// <summary>
    /// Models an MP3 file with its ID3V2 header, frame information, and data.
    /// </summary>
    public class MP3File
    {
        #region Properties

        /// <summary>
        /// The ID3V2 header of the file.
        /// </summary>
        public ID3V2Header Header;

        /// <summary>
        /// How large the ID3V2 tag is, in bytes.
        /// </summary>
        public int TagSize;

        /// <summary>
        /// The frames that this MP3 file contains.
        /// </summary>
        public List<Frame> Frames;

        /// <summary>
        /// The audio data from the MP3 file.
        /// </summary>
        public byte[] Data;

        #endregion

        #region Constructors

        public MP3File(ID3V2Header header, int tagSize, List<Frame> frames)
        {
            Header = header;
            TagSize = tagSize;
            Frames = frames;
        }

        public MP3File(ID3V2Header header, int tagSize, List<Frame> frames, byte[] data) 
            : this(header, tagSize, frames)
            => Data = data;

        #endregion

        #region Temporary methods for convenience

        /// <summary>
        /// Lookup method for checking if a particular tag exists by its
        /// human-readable name.
        /// </summary>
        /// <param name="idAlias"></param>
        /// <returns></returns>
        public Frame GetFrameByName(string idAlias)
        {
            foreach (var f in Frames)
                if (f.Name.Equals(idAlias, StringComparison.OrdinalIgnoreCase))
                    return f;

            return null;
        }

        /// <summary>
        /// Sets ALL frames whose Name matches the human-readable alias
        /// provided to the updated value.
        /// </summary>
        /// <param name="idAlias">the human-readable name of the frame. See constants defined in FrameInfo</param>
        /// <param name="newValue">the modified frame</param>
        public void SetFrameByName(string idAlias, Frame newValue)
        {
            for (var i = 0; i < Frames.Count; i++)
                if (Frames[i].Name.Equals(idAlias, StringComparison.OrdinalIgnoreCase))
                    Frames[i] = newValue;
        }

        #endregion
    }
}