﻿///////////////////////////////////////////////////////////////////////////////
// Program.cs
//
// Author      : Adam Likuski
// Description : Default Program class for starting the main form
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Windows.Forms;
using FinalProject.Forms;

namespace FinalProject
{
    public static class Program
    {
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ID3V2EditorForm());
        }
    }
}
