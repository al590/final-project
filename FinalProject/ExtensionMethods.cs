﻿///////////////////////////////////////////////////////////////////////////////
// ExtensionMethods.cs
//
// Author      : Adam Likuski
// Description : Houses all extension methods for the project
///////////////////////////////////////////////////////////////////////////////
using System;

namespace FinalProject
{
    /// <summary>
    /// Provides extension methods used elsewhere in the project.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Case-insensitively compares two strings (ordinally).
        /// </summary>
        /// <param name="s1">The base string</param>
        /// <param name="s2">Another string</param>
        /// <returns>true if they are equal, false otherwise</returns>
        public static bool EqualsIgnoreCase(this string s1, string s2) => 
            s1.Equals(s2, StringComparison.OrdinalIgnoreCase);
    }
}
