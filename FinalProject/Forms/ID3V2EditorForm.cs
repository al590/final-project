﻿///////////////////////////////////////////////////////////////////////////////
// ID3V2EditorForm.cs
//
// Author      : Adam Likuski
// Description : Provides an interface for editing ID3V2 tagged MP3 files
///////////////////////////////////////////////////////////////////////////////
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Forms;
using FinalProject.ID3V2;
using FinalProject.ID3V2.Frames;
using FinalProject.ID3V2.IO;

namespace FinalProject.Forms
{
    public partial class ID3V2EditorForm : Form
    {
        #region Fields

        /// <summary>
        /// Reference to the currently selected MP3 file.
        /// </summary>
        private string _filePath;

        /// <summary>
        /// An instance of the MP3File class to modify the mp3 loaded.
        /// </summary>
        private MP3File _file;

        #endregion

        #region Constructors

        public ID3V2EditorForm() 
            => InitializeComponent();

        #endregion

        #region Event Handlers

        private void OnOpenClicked(object sender, EventArgs e)
        {
            ofd.Filter = "MP3 Files (*.mp3) | *.mp3";
            ofd.Title = "Open Audio File";

            var result = ofd.ShowDialog();

            if (result != DialogResult.OK)
                return;

            _filePath = ofd.FileName;
            LoadFile(_filePath);
        }

        private void OnUpdateClicked(object sender, EventArgs e)
        {
            // Update any text frames that have been modified
            foreach (var t in _file.Frames)
            {
                // Image frame is updated immediately after loading a new album art
                // so we only need to worry about text frames
                if (!(t is TextFrame frame))
                    continue;

                if (frame.Name.EqualsIgnoreCase(FrameInfo.ALBUM))
                    frame.Text = txtAlbum.Text;
                else if (frame.Name.EqualsIgnoreCase(FrameInfo.ARTIST))
                    frame.Text = txtArtist.Text;
                else if (frame.Name.EqualsIgnoreCase(FrameInfo.TRACK_NAME))
                    frame.Text = txtTrackName.Text;
                else if (frame.Name.EqualsIgnoreCase(FrameInfo.TRACK_NUMBER))
                    frame.Text = txtTrackNumber.Text;
                else if (frame.Name.EqualsIgnoreCase(FrameInfo.YEAR))
                    frame.Text = txtYear.Text;
                else if (frame.Name.EqualsIgnoreCase(FrameInfo.GENRE))
                    frame.Text = txtGenre.Text;
            }

            try
            {
                using (var writer = new MP3Writer(_filePath))
                    writer.Write(_file);
            }
            catch (Exception ex)
            {
                Error($"Couldn't update MP3 file: {ex.Message}");
            }
        }

        private void OnAlbumArtClick(object sender, EventArgs e)
        {
            ofd.Filter = "Image files (*.jpg, *.jpeg, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.png";
            ofd.Title = "Open New Image";

            var result = ofd.ShowDialog();

            if (result != DialogResult.OK)
                return;

            try
            {
                // Check if the image is readable by trying to load it first
                var image = Image.FromFile(ofd.FileName);

                // If we've gotten this far we're fine to overwrite the album image
                var picFrame = _file.GetFrameByName(FrameInfo.ATTACHED_PICTURE) 
                    as AttachedPictureFrame 
                    ?? new AttachedPictureFrame
                {
                    Encoding = 0,
                    PictureType = 3,
                    Description = ""
                };

                // Only basic support again
                picFrame.MimeType = image.RawFormat.Guid == ImageFormat.Jpeg.Guid ? "image/jpeg" : "image/png";

                // Need to overwrite data buffer
                using (var buffer = new MemoryStream())
                {
                    image.Save(buffer, image.RawFormat);
                    picFrame.Data = buffer.ToArray();
                }

                // Save frame and update picturebox
                if (_file.GetFrameByName(FrameInfo.ATTACHED_PICTURE) != null)
                    _file.SetFrameByName(FrameInfo.ATTACHED_PICTURE, picFrame);
                else
                    _file.Frames.Add(picFrame);

                picAlbumArt.Image = image;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Image is unreadable! ({ex.Message})");
            }
        }

        #endregion

        #region Functions

        private void Error(string message) =>
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        public void LoadFile(string path)
        {
            try
            {
                using (var reader = new MP3Reader(path))
                    _file = reader.Read();

                // Reset UI 
                txtArtist.Text = "";
                txtTrackName.Text = "";
                txtAlbum.Text = "";
                txtGenre.Text = "";
                txtTrackNumber.Text = "";
                txtYear.Text = "";

                // Disable frames that don't exist
                txtArtist.Enabled = false;
                txtTrackName.Enabled = false;
                txtAlbum.Enabled = false;
                txtGenre.Enabled = false;
                txtTrackNumber.Enabled = false;
                txtYear.Enabled = false;

                foreach (var frame in _file.Frames)
                {
                    if (frame is TextFrame tf)
                    {
                        if (frame.Name.EqualsIgnoreCase(FrameInfo.ALBUM))
                        {
                            txtAlbum.Text = tf.Text;
                            txtAlbum.Enabled = true;
                        }
                        else if (frame.Name.EqualsIgnoreCase(FrameInfo.ARTIST))
                        {
                            txtArtist.Text = tf.Text;
                            txtArtist.Enabled = true;
                        }
                        else if (frame.Name.EqualsIgnoreCase(FrameInfo.TRACK_NAME))
                        {
                            txtTrackName.Text = tf.Text;
                            txtTrackName.Enabled = true;
                        }
                        else if (frame.Name.EqualsIgnoreCase(FrameInfo.TRACK_NUMBER))
                        {
                            txtTrackNumber.Text = tf.Text;
                            txtTrackNumber.Enabled = true;
                        }
                        else if (frame.Name.EqualsIgnoreCase(FrameInfo.YEAR))
                        {
                            txtYear.Text = tf.Text;
                            txtYear.Enabled = true;
                        }
                        else if (frame.Name.EqualsIgnoreCase(FrameInfo.GENRE))
                        {
                            txtGenre.Text = tf.Text;
                            txtGenre.Enabled = true;
                        }
                    }
                    else if (frame is AttachedPictureFrame apf)
                    {
                        try
                        {
                            using (var buffer = new MemoryStream(apf.Data))
                                picAlbumArt.Image = Image.FromStream(buffer);
                        }
                        catch (Exception ex)
                        {
                            Error($"Error loading album art - {ex.Message}");
                        }
                    }
                }

                // Enable saving
                btnUpdate.Enabled = true;
            }
            catch (Exception ex)
            {
                Error($"Exception occured while parsing file: {ex.Message}");
            }
        }

        #endregion
    }
}